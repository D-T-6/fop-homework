#!/bin/sh

folder=$(echo $1 | sd '.*\/(.*).git' '$1')
new_folder=$(echo $folder | sd 'kiu22w(.*)-kiu_tabatadze.dimitri' '$1')

git clone $1

mv $folder $new_folder

cd $new_folder

if [ -d "src" ]; then
  cd "src"
fi

nvim
