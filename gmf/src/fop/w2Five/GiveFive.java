package fop.w2Five;

public class GiveFive extends MiniJava {
    static int applyFive(int number) {
        switch (number % 5) {
            case 0:
                return number / 5;
            case 1:
                return number + 9;
            case 2:
                return number - 1;
            case 3:
                return number + 7;
            case 4:
                return number + 6;
        }
        return number;
    }

    public static void main(String[] args){
	    int number = readInt();

        int counter = 0;
        while (number > 1) {
            number = applyFive(number);
            counter++;
        }

        write(counter);
    }
}
