#!/bin/sh

dirname=$(basename $1)
echo $dirname

cd $dirname

if [ $(basename $(pwd)) == $dirname ]; then
  mv .git archived.git
  cd ..

  git add $dirname
  git commit -m "archived '$dirname/'"

  git push
fi
