package pgdp.arrays;

public class ProbabilisticSearch extends  MiniJava {
    /**
     * Binary search slightly modified from the lecture
     */
    public static int[] find (int[] a, int x) {
        return binarySearch(a, x, 0, a.length-1, 1);
    }

    public static int[] binarySearch (int[] a, int x, int n1, int n2, int numberOfSteps) {
        int t = (n1+n2)/2;
        if (a[t] == x)
            return new int[]{t, numberOfSteps};
        else if (n1 >= n2)
            return new int[]{-1, numberOfSteps};
        else if (x > a[t]) 
            return binarySearch (a,x,t+1,n2, numberOfSteps + 1);
        else if (n1 < t)
            return binarySearch (a,x,n1,t-1, numberOfSteps + 1);
        else 
            return new int[]{-1, numberOfSteps};
    }

    public static int[] probalisticSearch(int[] arr, int value) {
        int l = Math.round((float)(arr.length - 1) * (value - arr[0]) / (arr[arr.length - 1] - arr[0]));
        int r = l;

        int c = 1;
        while (arr[l] > value || value > arr[r]) {
            if (arr[l] < value) {
                l = r;
                r += 1 << c - 1;
            } else {
                r = l;
                l -= 1 << c - 1;
            }

            if (r >= arr.length) {
                r = arr.length - 1;
            } else if (l < 0) {
                l = 0;
            }
            c++;
        }

        return binarySearch(arr, value, l + 1, r - 1, c);
    }
    
    public static void compareApproaches(int[] arr, int min, int max) {
        long s1 = 0, s2 = 0;

        write("Binary Search:");

        int max1 = 0, max1y = -1;

        for (int v = min; v <= max; v++) {
            int[] res = find(arr, v);

            if (max1 < res[1]) {
                max1 = res[1];
                max1y = v;
            }
            s1 += res[1];
        }

        write("Maximum number of calls:");
        write(max1);
        write("Value at which the maximum number of calls occurs:");
        write(max1y);
        write("Number of total calls:");
        write(s1);

        write("Probabilistic Search: ");

        int max2 = 0, max2y = -1;

        for (int v = min; v <= max; v++) {
            int[] res = probalisticSearch(arr, v);

            if (max2 < res[1]) {
                max2 = res[1];
                max2y = v;
            }
            s2 += res[1];
        }

        write("Maximum number of calls:");
        write(max2);
        write("Value at which the maximum number of calls occurs:");
        write(max2y);
        write("Number of total calls:");
        write(s2);
    }

    public static void main(String[] args) {
        // Not part of the exercise but can be helpful for debugging purposes
        int[] exampleArray = new int[]{6, 20, 22, 35, 51, 54, 59, 74, 77, 80, 87, 94, 97};
        // int[] exampleArray = new int[]{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 100};

        // int[] res = probalisticSearch(exampleArray, 3);
        // int[] res = probalisticSearch(exampleArray, 74);
        // int[] res = probalisticSearch(exampleArray, 75);
        // write("[ " + res[0] + ", " + res[1] + " ]");
        compareApproaches(exampleArray, 6, 97);
    }
}
