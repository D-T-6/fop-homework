package fop.w5zoo;

public class Vivarium {
	private int area;
	private int constructionYear;
	private Animal[] inhabitants;
	
	public Vivarium(Animal[] inhabitants, int area, int constructionYear) {
		this.inhabitants = inhabitants;
		this.area = area;
		this.constructionYear = constructionYear;
	}

	public int getCosts() {
		int a = 0;
		for (Animal i : inhabitants) {
			a += i.getFoodCosts();
		}
		return a + 900 + this.area * 100 + this.area * (2021 - this.constructionYear) * 5;
	}

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();

		res.append("[area: ");
		res.append(this.area);
		res.append(", constructionYear: ");
		res.append(this.constructionYear);
		res.append(", animals: ");

		for (int i = 0; i < inhabitants.length; i++) {
			res.append(inhabitants[i].toString());
			res.append(i < inhabitants.length - 1 ? ", " : "]");
		}

		return res.toString();
	}
}
