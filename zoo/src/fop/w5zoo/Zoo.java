package fop.w5zoo;

public class Zoo {
	private Vivarium[] vivaria;

	public Zoo(Vivarium[] vivaria) {
		this.vivaria = vivaria;
	}

	public int getCosts() {
		int a = 0;

		for (Vivarium v : vivaria) {
			a += v.getCosts();
		}

		return a;
	}

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();

		res.append("{");

		for (int i = 0; i < vivaria.length; i++) {
			res.append(vivaria[i].toString());
			res.append(i < vivaria.length - 1 ? ", " : "}");
		}

		return res.toString();
	}
}
