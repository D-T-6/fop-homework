package fop.w5zoo;

public class Animal {
	private int foodCosts;
	private String name;

	public int getFoodCosts() {
		return foodCosts;
	}

	public String getName() {
		return name;
	}

	public Animal(String name, int foodCosts) {
		this.foodCosts = foodCosts;
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();

		result.append("(name: ");
		result.append(this.name);
		result.append(", foodCosts: ");
		result.append(this.foodCosts);
		result.append(")");

		return result.toString();
	}
}
